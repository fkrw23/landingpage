
function DarkTheme() { // Dark Modus, Steuert per jQuery-Selektoren CSS Elemente an


$('#Masthead').css('background','url("../img/bg-pattern-inverted.png"), black');
$('#mainNav').css('background','#000066');
$('#features').css('background','#101010');
$('#features').css('color','white');

}





function NormalTheme() { // Normaler Modus, Steuert per jQuery-Selektoren CSS Elemente an


$('#Masthead').css('background','url("../img/bg-pattern.png"), linear-gradient(to left, #7b4397, #dc2430)');
$('#mainNav').css('background','white')
$('#features').css('background','white');
$('#features').css('color','black');
}



function SprachEinstellung(){   // Funktion die bei onclick() aufgerufen wird um per ID die Textelemente neu zuzuweisen

var Sprache = $('#Sprachbutton').text();


if (Sprache=="Deutsch") {    // if-Abfrage zur Sprache
$('#Features').text("Funktionen");
$('#Features2').text("Funktionen");
$('#Frageapp').text("Die App um Fragen zu stellen");
$('#F1').text("Interaktiv");
$('#F2').text("Anonym");
$('#F3').text("Zum Mitmachen");
$('#F4').text("Mobil");
$('#IntroF').text("Sehe die Möglichkeiten im Vorlesungssaal");
$('#LF').text("Funktionen als Dozent");
$('#LF1').text("Fragen lesen");
$('#LF2').text("Fragen moderieren");
$('#LF3').text("Bonus Tokens für gute Fragen vergeben");
$('#SF').text("Funktionen als Student");
$('#SF1').text("Stelle Fragen anonym");
$('#SF2').text("Lese Fragen von Komillitonen");
$('#SF3').text("Verdienen sich sich Bonus Tokens durch gute Fragen");
$('#Sprachbutton').text("English");

}

if (Sprache=="English") {    
$('#Features').text("Features");
$('#Features2').text("Features");
$('#Frageapp').text("The question app");
$('#F1').text("Interactiv");
$('#F2').text("Anonymous");
$('#F3').text("Engaging");
$('#F4').text("Mobile");
$('#IntroF').text("See what you can do in the lecture hall");
$('#LF').text("Lecturer features");
$('#LF1').text("Read questions");
$('#LF2').text("Moderate questions");
$('#LF3').text("Award Bonus Tokens for good questions");
$('#SF').text(" Student features");
$('#SF1').text("Put questions anonymously");
$('#SF2').text("Read questions from other participants");
$('#SF3').text("Earn tokens for good questions");
$('#Sprachbutton').text("Deutsch");

}

}
